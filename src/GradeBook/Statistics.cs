﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeBook
{
    public class Statistics
    {
        // instance vars
        public double low;
        public double high;
        public double average;

        // default constructor
        public Statistics() { }

        // constructor with args
        public Statistics(double low, double high, double average)
        {
            this.low = low;
            this.high = high;
            this.average = average;
        }
    }
}
