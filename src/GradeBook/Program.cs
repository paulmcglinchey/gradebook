using System;
using System.Collections.Generic;

namespace GradeBook
{
    class Program
    {
        public static void Main(string[] args)
        {
            var book = new Book("Stage 1", new List<double>() { 20.3, 54.2, 82.1, 73.5, 77.3 });

            book.showStats();
        }
    }
}