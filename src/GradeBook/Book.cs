using System;
using System.Collections.Generic;

namespace GradeBook
{
    public class Book
    {
        // instance vars
        private string name;
        private List<double> grades;

        // default constructor
        public Book() { }

        // constructor with args, taking a string as name and a list of doubles as grades
        public Book(string name, List<double> grades)
        {
            this.setName(name);
            this.setGrades(grades);
        }

        // getters and setters
        public string getName()
        {
            return this.name;
        }
        public void setName(string name)
        {
            if (name.Trim().Length > 0 && name.Length < 255)
            {
                // checking if the passed string is of valid length
                this.name = name;
            } else
            {
                this.name = "DEFAULT";
                throw new ArgumentException("NAME is not of valid length, set to DEFAULT");
            }
        }

        public List<double> getGrades()
        {
            return this.grades;
        }
        public void setGrades(List<double> grades)
        {
            if (grades.Count > 0)
            {
                // checking that the passed list has entries
                this.grades = grades;
            } else
            {
                this.grades = new List<double>();
                throw new ArgumentException("GRADES must be a valid list of doubles, set to EMPTY list");
            }
        }

        // methods
        public void addGrade(double grade)
        {
            if (this.getGrades() != null)
            {
                this.grades.Add(grade);
            } else
            {
                setGrades(new List<double>() { grade });
            }
        }

        /**
         * Returns a dictionary of the stats for this instance of the grades
         */
        public Statistics GetStats()
        {
            // setting all the results to boundary values, actual values will replace them iteratively
            var highGrade = Double.MinValue;
            var lowGrade = Double.MaxValue;
            var total = 0.0;
            double avg;

            foreach(var grade in this.grades)
            {
                highGrade = grade > highGrade ? grade : highGrade;
                lowGrade = grade < lowGrade ? grade : lowGrade;

                total += grade;
            }

            // getting the average of the grades
            avg = total / this.grades.Count;

            return new Statistics(lowGrade, highGrade, avg);
        }

        /**
         * Displays the stats of this instance of the gradebook object
         */
        public void showStats()
        {
            var stats = this.GetStats();

            Console.WriteLine($"{this.getName()}\nLowest: {stats.low}, Highest: {stats.high}, Average: {stats.average}");
        }
    }
}