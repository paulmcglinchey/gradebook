using System;
using Xunit;

namespace GradeBook.Tests
{
    public class BookTests
    {
        [Fact]
        public void Test1()
        {
            // arrange
            var book = new Book();
            book.addGrade(89.1);
            book.addGrade(90.5);
            book.addGrade(92.1);

            // act
            var result = book.GetStats();

            // assert
            Assert.Equal(90.6, result.average, 0);
            Assert.Equal(89.1, result.low, 0);
            Assert.Equal(92.1, result.high, 0);
        }
    }
}
